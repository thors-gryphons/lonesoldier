using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool gameEnds = false;
    float canvasDelay = 2f;
    public GameObject gameOverCanvas;
    

    private void Start()
    {
        gameOverCanvas.SetActive(false);
        
        Time.timeScale = 1;
    }
    public void Playgame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void GameOver()
    {
        if (gameEnds == false)
        {
            gameEnds = true;
            Invoke("showCanvas", canvasDelay);
        }
    }
    public void showCanvas()
    {
        gameOverCanvas.SetActive(true);
        Time.timeScale = 0;

    }
    public void replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void backmenu()
    {
        SceneManager.LoadScene(0);
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
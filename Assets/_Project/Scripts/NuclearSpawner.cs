using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuclearSpawner : MonoBehaviour
{
	private void Start()
	{
		InvokeRepeating("Spawn", 10f, 30);
	}
	public GameObject NuclearPrefab;
	Vector3 RandomCircle(Vector3 center, float radius)
	{
		float ang = Random.value * 360;
		Vector3 pos;
		pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
		pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
		pos.z = center.z;
		return pos;
	}
	void Spawn()
	{
		Vector2 position = RandomCircle(Vector3.zero, 4);
		Instantiate(NuclearPrefab, position, Quaternion.identity);
	}
}

using TMPro;
using UnityEngine;

public class ChronoMeter : MonoBehaviour
{
    float timer;
    public TextMeshProUGUI TimerText;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
       TimerText.text = Mathf.Floor(timer).ToString();
    }
}

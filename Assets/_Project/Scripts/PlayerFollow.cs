using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{
    public Transform target;
    void Update()
    {
       
        
        if(target == null)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.position = target.transform.position;
        }
    }
}

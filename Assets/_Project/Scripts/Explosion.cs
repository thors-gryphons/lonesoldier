using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public GameObject[] ZnabiXplosion;

    private void Update()
    {
        ZnabiXplosion = GameObject.FindGameObjectsWithTag("Zombie");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            foreach(GameObject Zombie in ZnabiXplosion)
            {
                Destroy(Zombie);
            }
            Destroy(gameObject);    
        }

    }
}

using UnityEngine.UI; 
using UnityEngine;

public class HealthManager : MonoBehaviour
{
   public GameManager gameManager;
    public Image healthbar;
    public float currentHealth = 100f;
    public float fullHealth;
    private void Start()
    {
        currentHealth = fullHealth;
    }
    public void AddDamage(float damage)
    {
        currentHealth -= damage;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        healthbar.fillAmount = currentHealth / 100f;
        if(currentHealth<= 0)
        {
            Die();
            gameManager.GameOver();
        }
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Zombie")
        {
            AddDamage(20);
        }
    }
    public void Die()
    {
         Destroy(this.gameObject);
    }
}